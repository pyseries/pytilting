from ase.spacegroup import *

def my_get_spacegroup(*args,**kwargs):
        """
        This function provides a thin wrapper to ASE's **get_spacegroup** function because this function had changed before.
		"""
        # For older version of ase.
        # return get_spacegroup(*args,**kwargs)
        return get_spacegroup(*args)

